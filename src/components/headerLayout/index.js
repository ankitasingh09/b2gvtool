import { useOktaAuth } from "@okta/okta-react";
import React from "react";
import logo from "../../assets/logo.svg";
import "./index.css";

const HeaderLayout = () => {
  const authState = useOktaAuth()?.authState?.idToken?.claims?.name;
  const userName=authState?.split(" ");
  return (
    <>
      <div className="header-wrapper">
        <div className="header-wrapper-content">
          <img style={{height:'2rem',width:'2rem'}} alt="logo" src={logo}/>
          <div className="header-text">B2GV</div>
        </div>
        <div>
          <div className="login-display">{userName?.[0]?.charAt(0)}{userName?.[1]?.charAt(0)}</div>
        </div>
      </div>
    </>
  );
};

export default HeaderLayout;
