import React from "react";
import { Drawer, Typography } from "@mui/material";
import styled from "styled-components";
import { Close } from "@mui/icons-material";

const SdDrawer = styled(Drawer)`
  .MuiBackdrop-root {
    opacity: 0.1 !important;
    position: relative;
  }
  .MuiDrawer-paper {
    position: absolute;
    width: 444px;
    box-shadow: -6px 6px 10px rgba(92, 90, 139, 0.2);
    padding: 40px 24px;
  }
`;

function ToolTipDrawer({ open, closeDrawer, data, setOpen }) {
  return (
    <div>
      {
        <React.Fragment key={"key"}>
          <SdDrawer
            anchor={"right"}
            open={open}
            onClose={() => setOpen(false)}
            style={{ top: "4rem" }}
            //  variant="persistent"
          >
            <Typography
              sx={{
                fontFamily: "DM Sans",
                fontStyle: "normal",
                fontWeight: "500",
                fontSize: "25px",
                lineHeight: "32px",
                textAlign: "start",
                letterSpacing: "2px",
                textTransform: "uppercase",
                color: "#1E2436",
              }}
            >
              More Info
            </Typography>
            <Close
              sx={{
                fontWeight: 700,
                position: "absolute",
                top: "1rem",
                right: "1rem",
                color: "red",
                cursor: "pointer",
              }}
              onClick={() => setOpen(false)}
            />
            <div
              className="drawer-data"
              style={{
                display: "flex",
                flexDirection: "column",
                gap: "24px",
                alignItems: "start",
                marginTop: "48px",
              }}
            >
              <Typography
                sx={{
                  fontFamily: "DM Sans",
                  fontStyle: "normal",
                  fontWeight: "500",
                  fontSize: "17px",
                  lineHeight: "22px",
                }}
              >
                {data?.label}
              </Typography>
              <Typography
                align="left"
                sx={{
                  fontFamily: "DM Sans",
                  fontStyle: "normal",
                  fontWeight: "normal",
                  fontSize: "14px",
                  lineHeight: "24px",
                }}
              >
                {data?.tooltipContent}
              </Typography>
            </div>
          </SdDrawer>
        </React.Fragment>
      }
    </div>
  );
}

export default ToolTipDrawer;
