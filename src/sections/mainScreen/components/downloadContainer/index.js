import React from "react";
import downloadicon from "../../../../assets/downloadicon.svg";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const DownloadContainer = ({ authError, setAuthError }) => {
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setAuthError(false);
  };
  return (
    <div className="sanitizationBox">
      <div>
        <div className="mainBox">
          <img src={downloadicon} alt="logo" className="checkimage"></img>
          <div className="percentSanitized">Download Link Available For</div>
          <div className="sanitizationComplete">72 Hours</div>
        </div>
      </div>
      <Snackbar open={authError} autoHideDuration={3000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error" sx={{ width: "100%" }}>
          Authentication failed!
        </Alert>
      </Snackbar>
    </div>
  );
};

export default DownloadContainer;
