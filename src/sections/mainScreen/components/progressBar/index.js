import React from "react";
import "./index.css";
import group from "../../../../assets/Group 35851.png";

//const [statusField,setStatusField]=useState({"Import":"completed","Sanitize":"active","Download":"inactive"})

const ProgressBar = (props) => {
  const { statusField } = props;

  return (
    <div className="mainProgressWrapper">
      <div className="progressWrappers">
        {statusField["Import"] === "active" && (
          <div>
            <div className="checkMarkWrapper">
              <div className="blueMark"></div>
              <div>Import</div>
            </div>
            <div className="firstBarGrey"></div>
          </div>
        )}
        {statusField["Import"] === "completed" && (
          <div>
            <div className="checkMarkWrapper">
              <img src={group} alt="logo" className="checkmark1"></img>
              <div>Import</div>
            </div>
            <div className="blueBar"></div>
          </div>
        )}

        {statusField["Sanitize"] === "active" && (
          <div>
            <div className="checkMarkWrapper2">
              <div className="checkMark2"></div>
              <div>Sanitize</div>
            </div>
          </div>
        )}

        {statusField["Sanitize"] === "completed" && (
          <div>
            <div className="checkMarkWrapper2">
              <img src={group} alt="logo" className="checkmark1"></img>
              <div>Sanitize</div>
            </div>
          </div>
        )}

        {statusField["Sanitize"] === "inactive" && (
          <div>
            <div className="checkMarkWrapper2">
              <div className="greyMark"></div>
              <div>Sanitize</div>
            </div>
          </div>
        )}
      </div>

      <div className="progressWrappers">
        {statusField["Sanitize"] === "inactive" && (
          <div>
            <div className="greyBar"></div>
          </div>
        )}
        {statusField["Sanitize"] === "active" && (
          <div>
            <div className="greyBar"></div>
          </div>
        )}
        {/* {statusField["Sanitize"] === "inactive" && (
          <div>
            <div className="greyBar"></div>
          </div>
        )} */}

        {statusField["Sanitize"] === "completed" && (
          <div>
            <div className="blueBar"></div>
          </div>
        )}
        {statusField["Download"] === "inactive" && (
          <div>
            <div className="checkMarkWrapper3">
              <div className="greyMark"></div>
              <div>Download</div>
            </div>
          </div>
        )}
        {statusField["Download"] === "active" && (
          <div>
            <div className="checkMarkWrapper3">
              <div className="blueMark"></div>
              <div>Download</div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default ProgressBar;
