import React, { useState } from "react";
import { useOktaAuth } from "@okta/okta-react";
import PropTypes from "prop-types";
import rightIcon from "../../../../assets/rightArrowIcon.svg";
import "./index.css";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import formatJsonData from "../../../../utils/formatJsonData";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";
import { Gitlab } from '@gitbeaker/browser'; // All Resources
import { Projects } from '@gitbeaker/browser'; // Just the Project Resource

const SubHeader = ({ buttonState, setButtonState, setStatusField, fields, errorState, setErrorState }) => {
  const history = useHistory();
  const authStateEmail = useOktaAuth()?.authState?.idToken?.claims?.email;
 console.log("check",authStateEmail);
  const [loader, setLoader] = useState(false);
  const fetchPostApiToSanitizeData = () => {
    let formData = new FormData();
    const [bitbucketRepo, directoryStructure, Microservice] = fields;
    const url =process.env.REACT_APP_TRIGGER_SANITIZE_URL;
    const formatJson = formatJsonData(bitbucketRepo, directoryStructure, Microservice);
    const repoNameUrl = bitbucketRepo.content[0].value.substring(bitbucketRepo.content[0].value.lastIndexOf("/") + 1);
    const repoName = repoNameUrl?.split(".")?.[0];

    //Appending data to it
    formData.append("token", process.env.REACT_APP_TOKEN);
    formData.append("ref", "tmo/main");
    formData.append("JSON_DATA", formatJson);
    formData.append("REPO_NAME", repoName);
    formData.append("REPO_URL", bitbucketRepo.content[0].value);
    formData.append("BRANCH", bitbucketRepo.content[1].value);
    formData.append("accesstoken", bitbucketRepo.content[2].value);
    formData.append("EMAIL", authStateEmail);

    //Config for axios POST request
    const config = {
      headers: { "content-type": "multipart/form-data" },
    };

    axios
      .post(url, formData, config)
      .then((response) => {
        setLoader(false);
        const pipelineId = response?.data?.id;
        const projectId = response?.data?.project_id;
        setButtonState({
          name: "CONTINUE",
          disabledState: false,
        });
        setStatusField({
          Import: "completed",
          Sanitize: "active",
          Download: "inactive",
        });
        history.replace({ pathname: "/sanitize", search: `?pipelineId=${pipelineId}&projectId=${projectId}`, state: { isActive: true } });
      })
      .catch((error) => {
        setLoader(false);
        setButtonState({ name: "SANITIZE", disabledState: false });
        setStatusField({
          Import: "active",
          Sanitize: "inactive",
          Download: "inactive",
        });
        setErrorState(true);
      });
  };

  const updateButtonState = () => {
    if (buttonState?.name === "IMPORT REPO") {
      setButtonState({ name: "SANITIZE", disabledState: false });
      setStatusField({
        Import: "active",
        Sanitize: "inactive",
        Download: "inactive",
      });
    }
    if (buttonState?.name === "SANITIZE") {
      fetchPostApiToSanitizeData();
      setLoader(true);
    }
    if (buttonState?.name === "CONTINUE") {
      setButtonState({ name: "DOWNLOAD", disabledState: false });
      setStatusField({
        Import: "completed",
        Sanitize: "completed",
        Download: "active",
      });
    }
    if(buttonState?.name==="DOWNLOAD"){
      fetchArtifactsDownload();
    }
  };

  const fetchArtifactsDownload= ()=>{
    console.log("pppp",process.env);
    const api = new Gitlab({
      host: 'https://gitlab.com',
      token: process.env.REACT_APP_DOWNLOAD_ARTIFACTS_TOKEN,
    });
    // api.Projects.all().then((projects) => {
    //   console.log(projects);
    // });
    
    //  api.Jobs.downloadSingleArtifactFile(
    //   34166993,
    //   2182592458,
    //   'artifacts.zip',
    //   {
    //     stream: false
    //   }
    // ).then(response=>console.log({response}));

    // // api.Jobs.then(res=>console.log(res));

    const url=`https://gitlab.com/api/v4/projects/${process.env.REACT_APP_GITLAB_PROJECT_ID}/jobs/2182592460/artifacts`;

    fetch(url, {
      method: 'GET',
      headers: {
      'Access-Control-Allow-Origin': '*' ,
      'PRIVATE-TOKEN':process.env.REACT_APP_DOWNLOAD_ARTIFACTS_TOKEN,
   },
   }).then(res=>console.log({res}));
   

   

 



}

  return (
    <div className={`subHeader-wrapper ${buttonState?.name === "IMPORT REPO" ? "showBorders" : ""} display-flex`}>
      <div className="subheader-header">
        {buttonState?.name === "IMPORT REPO" && (
          <div className="display-flex header-content">
            <div className="header-styling">SANITIZE AN ASSET</div>
            <div className="display-flex">
              <div className="sub-style-header">
                DOCUMENTATION
                <Link
                  className="display-flex"
                  to={{
                    pathname: "https://confluencesw.t-mobile.com/display/ENVX/Bitbucket+to+Vault+and+Gitlab",
                  }}
                  target="_blank"
                >
                  <img alt="rightIcon" src={rightIcon} className="rightIconStyle"></img>
                </Link>
              </div>
              <div className="sub-style-header">
                SAMPLE INI
                <Link
                  className="display-flex"
                  to={{
                    pathname: "https://confluencesw.t-mobile.com/display/ENVX/B2GV+tool+UI+-+Design+Requirements",
                  }}
                  target="_blank"
                >
                  <img alt="right-icon" src={rightIcon} className="rightIconStyle"></img>
                </Link>
              </div>
            </div>
          </div>
        )}
        {buttonState?.name === "SANITIZE" && <div className="header-styling">REPO IMPORTED</div>}
        {buttonState?.name === "CONTINUE" && <div className="header-styling">Sanitization</div>}
        {buttonState?.name === "DOWNLOAD" && <div className="header-styling">Sanitization</div>}
      </div>
      {buttonState?.name !== "CONTINUE" && (
        <div className="button-wrapper">
          <button className={`button-display ${buttonState.disabledState ? "disable-button" : ""}`} onClick={() => updateButtonState()}>
            {buttonState?.name}
          </button>
        </div>
      )}
      <Backdrop sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }} open={loader}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
};

SubHeader.propTypes = {
  buttonState: PropTypes.object,
  setButtonState: PropTypes.func,
};
export default SubHeader;
