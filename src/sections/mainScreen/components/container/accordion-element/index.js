import React, {useState } from "react";
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Button,
} from "@mui/material";
import styled from "styled-components";
import CompleteButton from "../completeButton";
import IncompleteButton from "../incompleteButton";
import InputField from "../InputField";
import SubAccordion from "../subAccordion";
import DropDown from "../dropDown";

const SdAccordion = styled(Accordion)`
  &.microservice-accordion .add-microservice {
    justify-self: flex-start;
    align-self: flex-start;
    font-weight: 700;
  }
  border-radius: 10px !important;
  padding: 0 20px;
  min-height: 96px;
  display: flex;
  flex-direction: column;
  justify-content: center;

  .MuiAccordionSummary-content,
  .MuiAccordionSummary-root {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;
const AccordionHeader = styled("div")`
  font-family: DM Sans;
  font-size: 25px;
  font-weight: 500;
  line-height: 32px;
  letter-spacing: 2px;
  text-transform: uppercase;
`;

const ButtonWrapper = styled("div")`
  display: flex;
  width: 100%;
  justify-content: end;
`;

function Index(props) {
  const {
    data,
    index,
    activeAccordion,
    activateNext,
    setActiveAccordion,
    isMicroservicesAccordion,
    openDrawer,
    fields,
    setFields,
    header,
    buttonState,
    setButtonState
  } = props;
  
  const [noOfMicroservices, setNoOfMicroservices] = useState(1);
    

  const addMicroservice = () => {
    setNoOfMicroservices((val) => val + 1);
    const newData={
      [`Microservice${noOfMicroservices+1}`]:[{
        "label": "Name",
        "value": "",
        "subAccordion":true,
        "filterBy":"MICROSERVICES",
        "error":false,
        "tooltipContent": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil aut nostrum explicabo reprehenderit optio amet ab temporibus asperiores quasi cupiditate. Voluptatum ducimus voluptates voluptas?"
      },
      {
        "label": "Path 1",
        "value": "",
        "subAccordion":true,
        "filterBy":"MICROSERVICES",
        "error":false,
        "tooltipContent": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil aut nostrum explicabo reprehenderit optio amet ab temporibus asperiores quasi cupiditate. Voluptatum ducimus voluptates voluptas?"
      }
      ]};
    
      data.content.push(newData);
      const field=[...fields];
      field[2]=data;
      setFields(field);
  };

  function removeMicroservice(index) {
    const fieldState=[...fields];
    fieldState[2].content.splice(index-1,1);
    setFields(fieldState);
  }

  const handleChange = (e, value,state,filter) =>{
    if(!state){
      const fieldMap=[...fields];
     const stateCondition= fieldMap
      .filter(item=>item.header===filter)
      stateCondition?.[0].content
      .filter(each=>{
        if(each.label===value) {
          each.value=e.target.value;
          if(each.value===""){
            each.error=true;
          }else{
            each.error=false;
          }
        }
        return null;
      })
     setFields(fieldMap);
    }
    // setFields((val) => ({ ...val, [value]: e.target.value }));
  }
  let i = 0;

  const handleContinue = () => {
    checkCompleteStatus();
    checkButtonStatus();
    if (!validateFields()) return;
    activateNext();
  };

  const validateFields = () => {
    let res = true;
    const field=[...fields];
    if(!isMicroservicesAccordion){
    field.filter(item=>{
      if(item.header===header){
        item.content.forEach(content=>{
          if(content.value===""){
            content.error=true;
          }else{
            content.error=false;
          }
        })
      }
      return null;
    })
    setFields(field);
    field.filter(item=>{
      if(item.header===header){
        item.content.forEach(content=>{
          if(content.error){
            res=false;
            return;
          }
        })
      }
      return null;
    })
    }else{
      field.filter(item=>{
        if(item.header===header){
          item.content.forEach(content=>{
            Object.values(content).forEach(condition=>{
              condition.forEach(iterate=>{
                if(iterate.value===""){
                  iterate.error=true;
                }else{
                  iterate.error=false;
                }
              })
             
            })
           
          })
        }
        return null;
      })
      setFields(field);
      field.filter(item=>{
        if(item.header===header){
          item.content.forEach(content=>{
            Object.values(content).forEach(service=>{
              service.forEach(serve=>{
                if(serve.error){
                  res=false;
                  return;
                }
              })
             
            })
            
          })
        }
        return null;
      })

    }
    return res;
  };

  const accordionHandleChange = (index) => (e, isExpand) => {
    setActiveAccordion(index);
  };

 const checkCompleteStatus=()=>{
   let completeStatus=true;
   const field=[...fields];
   if(!isMicroservicesAccordion){
   field.filter(item=>{
     if(item.header===header){
       item.content.forEach(content=>{
         if(content.value===""){
           completeStatus=false;
           item.status=false;
           return;
         }else{
           item.status=true;
         }
       })
     }
     return null;
   })
  
  }else{
    field.filter(item=>{
      if(item.header===header){
        item.content.forEach(contents=>{
          Object.values(contents).forEach(service=>{
            service.forEach(serve=>{
              if(serve.value===""){
                completeStatus=false;
                item.status=false;
                return;
              }else{
                item.status=true;
              }
            })
          
          })
        })
      }
      return null;
    })
  }
  setFields(field);
return completeStatus;
 } 

 const checkButtonStatus=()=>{
  const buttonConditon={...buttonState}; 
  let condition=false;
fields.filter(item=>{
   if(!item.status){
    condition=true;
    return;
   } 
   return null; 
});

if(!condition){
  setButtonState({
    ...buttonConditon,
    disabledState:false,
  })
}
 }


  return (
    <SdAccordion
      sx={{
        width: "800px",
        border: "none",
        boxShadow: "0px 10px 20px -10px rgba(0,0,0,0.4)",
      }}
      expanded={index === activeAccordion || false}
      onChange={accordionHandleChange(index)}
      key={index}
    >
      <AccordionSummary
        sx={{ justifyContent: "space-between" }}
        aria-controls="panel1a-content"
        id="panel1a-header"
        key={index}
        // onClick={() => setExpanded((val) => !val)}
        // onClick={() => seti((val) => val + 1)}
      >
        <AccordionHeader>{data.header}</AccordionHeader>
        {data.status ? <CompleteButton /> : <IncompleteButton />}
      </AccordionSummary>
      <AccordionDetails
        sx={{ display: "flex", flexDirection: "column", gap: "16px" }}
      >
        {!isMicroservicesAccordion
          ? data.content.map((item) => (
              (item.label!=="Environment"&&item.label!=="Log Level")?<InputField
                key={i++}
                value={item.value}
                handleChange={handleChange}
                fields={fields}
                error={item.error}
                data={item}
                openDrawer={openDrawer}
              />:<DropDown
              key={i++}
              value={item.value}
              handleChange={handleChange}
              fields={fields}
              setFields={setFields}
              error={item.error}
              filter={"BITBUCKET REPO INFO"}
              data={item}
              openDrawer={openDrawer}
              />
            ))
          : data.content.map((item,index)=>(<SubAccordion
          index={index+1}
          data={data?.content?.[index+0]||data?.content?.[0]}
          removeMicroservice={removeMicroservice}
          openDrawer={openDrawer}
          fields={fields}
          setFields={setFields}
        />))}

        {isMicroservicesAccordion ? (
          <Button
            className="add-microservice"
            style={{ alignSelf: "start", fontWeight: 700 }}
            onClick={addMicroservice}
          >
            + Add Microservice
          </Button>
        ) : null}
        <ButtonWrapper>
          <Button variant="contained" onClick={() => handleContinue()}>
            Continue
          </Button>
        </ButtonWrapper>
      </AccordionDetails>
    </SdAccordion>
  );
}

export default Index;
