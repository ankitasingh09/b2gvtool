import React from "react";
import PropTypes from "prop-types";
import "./index.css";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import editicon from "../../../../../assets/edit-icon.svg";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const SanitizeContainer = ({ fields, setButtonState, errorState, setErrorState }) => {
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setErrorState(false);
  };

  return (
    <div className="fieldsWrapper">
      <div
        className="editButton"
        onClick={() => {
          setButtonState({ name: "IMPORT REPO", disabledState: false });
        }}
      >
        <img src={editicon} alt="Edit!" className="editImage"></img>
        <div>Edit</div>
      </div>
      {fields.map((field) => {
        return (
          <>
            <div className="fieldHeaderContainer">{field.header}</div>

            {field.header !== "MICROSERVICES" ? (
              <div className="fieldValueWrapper">
                {field.content.map((stat) => {
                  return (
                    <>
                      <div className="fieldValue">
                        {stat.label}: {stat.value}
                      </div>
                    </>
                  );
                })}
              </div>
            ) : (
              <div>
                {field.content.map((state) => {
                  return (
                    <>
                      {Object.values(state).map((item) => {
                        return item.map((context) => {
                          return context.label === "Name" ? (
                            <div className="fieldValue">Microservice Name: {context.value}</div>
                          ) : (
                            <div className="fieldValue">
                              {context.label}: {context.value}
                            </div>
                          );
                        });
                      })}
                    </>
                  );
                })}
              </div>
            )}
          </>
        );
      })}
      {/* Error handling */}
      <Snackbar open={errorState} autoHideDuration={3000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error" sx={{ width: "100%" }}>
          Something went wrong in the Sanitize API!
        </Alert>
      </Snackbar>
    </div>
  );
};

SanitizeContainer.propTypes = {
  fields: PropTypes.array,
};

export default SanitizeContainer;
