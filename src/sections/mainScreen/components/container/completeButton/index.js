import React from 'react'
import CheckCircleRoundedIcon from '@mui/icons-material/CheckCircleRounded';
import styled from 'styled-components'
import { Typography } from '@mui/material';

const Wrapper = styled.div`
display:flex;
gap:8px;
align-items:center;
justify-self:end;
`

function CompleteButton() {
    return (
       <Wrapper>
           <CheckCircleRoundedIcon sx={{color:'green'}} />
           <Typography>Complete</Typography>
       </Wrapper> 

    )
}

export default CompleteButton
