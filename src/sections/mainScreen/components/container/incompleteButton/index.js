import React from 'react'
import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined';
import styled from 'styled-components'
import { Typography } from '@mui/material';

const Wrapper = styled.div`
display:flex;
gap:8px;
align-items:center;
justify-self:end;
`

function CompleteButton() {
    return (
       <Wrapper>
           <CircleOutlinedIcon />
           <Typography>Incomplete</Typography>
       </Wrapper> 

    )
}

export default CompleteButton