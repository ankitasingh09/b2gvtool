import React, { useContext, useEffect, useState } from "react";
import { TextField } from "@mui/material";
import { Typography } from "@mui/material";
import styled from "styled-components";
import Context from "../../../../../context/context";
import { Box } from "@mui/system";
import DeleteIcon from "../deleteIcon";
import Info from "@mui/icons-material/Info";
import ToolTipDrawer from "../../drawer/drawer";
const Wrapper = styled("div")`
  display: flex;
  flex-direction: column;
  margin-bottom: 8px;
  .info-icon {
    width: 16px;
    height: 16px;
    color: #9aa4bf;
    &:hover {
      color: #000;
    }
  }

  input {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 12px 12px;
  }
`;
function InputField(props) {
  const { data, inSubAccordion, openDrawer } = props;
  const [open, setOpen] = useState(false);
  const error = props.error;

  return (
    <Wrapper>
      <div
        style={{
          alignSelf: "start",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          gap: "12px",
          marginBottom: "2px",
        }}
      >
        <Typography
          sx={{
            fontSize: "17px",
            lineHeight: "22px",
            fontWeight: "500",
            alignSelf: "start",
          }}
        >
          {data.label}
        </Typography>
        <Info className="info-icon" onClick={() => setOpen(true)} />
        <ToolTipDrawer open={open} data={data} setOpen={setOpen} />
      </div>
      <Box
        sx={{
          ".MuiTextField-root": { width: "100%" },
          display: "flex",
          background: "#F4F6FA",
          alignItems: "center",
          paddingRight: "8px",
          svg: {
            color: "red",
          },
        }}
      >
        <TextField
          variant="filled"
          placeholder="The Placeholder"
          error={error ? true : false}
          InputProps={error ? null : { disableUnderline: true }}
          helperText={error ? "This field cannot be left empty" : null}
          value={props.value}
          onChange={(e) =>
            props.handleChange(e, data.label, data.subAccordion, data.filterBy)
          }
          sx={{
            ".MuiFilledInput-root": {
              background: inSubAccordion ? "#fff" : "#F4F6FA",
            },
          }}
        ></TextField>
        {inSubAccordion && data.label !== "Name" && props.position !== 1 && (
          <DeleteIcon
            value={data.value}
            label={data.label}
            microserviceName={props?.microserviceName}
            deletePath={props.deletePath}
          />
        )}
      </Box>
    </Wrapper>
  );
}

export default InputField;
