import React from "react";
import MenuItem from "@mui/material/MenuItem";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { Typography } from "@mui/material";
import styled from "styled-components";
import Info from "@mui/icons-material/Info";
import "./index.css";
const Wrapper = styled("div")`
  display: flex;
  flex-direction: column;
  margin-bottom: 8px;
  .info-icon {
    width: 16px;
    height: 16px;
    color: #9aa4bf;
    &:hover {
      color: #000;
    }
  }

  input {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 12px 12px;
  }
`;

const DropDown = ({ value, fields, setFields, filter, openDrawer, data }) => {
  const handleChange = (event) => {
    const fieldMap = [...fields];
    const stateCondition = fieldMap.filter((item) => item.header === filter);
    // stateCondition?.[0].content.forEach((each) => {

    // })

    stateCondition?.[0].content.forEach((each) => {
      if (each.label === data.label) {
        each.value = event.target.value;
        if (each.value === "") {
          each.error = true;
        } else {
          each.error = false;
        }
      }
    });
    setFields(fieldMap);
  };
  return (
    <Wrapper>
      <div
        style={{
          alignSelf: "start",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          gap: "12px",
          marginBottom: "2px",
        }}
      >
        <Typography
          sx={{
            fontSize: "17px",
            lineHeight: "22px",
            fontWeight: "500",
            alignSelf: "start",
          }}
        >
          {data.label}
        </Typography>
        <Info className="info-icon" onClick={() => openDrawer(data)} />
      </div>
      <FormControl
        sx={{
          ".MuiFormControl-root": { margin: "0px" },
          m: 1,
          minWidth: 120,
          background: "#F4F6FA",
          border: "0px",
        }}
      >
        <Select value={value} onChange={handleChange} displayEmpty inputProps={{ "aria-label": "Without label" }}>
          <MenuItem disabled value="">
            <div>The Placeholder</div>
          </MenuItem>
          {/* <MenuItem value={"dev"}>dev</MenuItem>
          <MenuItem value={"stage"}>stage</MenuItem>
          <MenuItem value={"prod"}>prod</MenuItem>
          <MenuItem value={"pre-prod"}>pre-prod</MenuItem> */}
          {data.dropDownData.map((condition) => (
            <MenuItem value={condition}>{condition}</MenuItem>
          ))}
        </Select>
        {data.error && <FormHelperText error={true}>This field cannot be left empty</FormHelperText>}
      </FormControl>
    </Wrapper>
  );
};

export default DropDown;
