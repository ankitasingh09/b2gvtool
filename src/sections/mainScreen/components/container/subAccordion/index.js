import React, { useEffect, useState, useContext } from "react";
import {
  Accordion,
  AccordionSummary,
  Typography,
  AccordionDetails,
  Button,
} from "@mui/material";
import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
import InputField from "../InputField";
import DeleteIcon from "../deleteIcon";


function Index(props) {
  const {fields,index,setFields}=props;
  const [pathIndex,setPathIndex]=useState(1);
  const [data, setData] = useState([
    {
      label: "Name*",
      value: "name",
    },
    {
      label: "Path 1",
      value: "path1",
    },
  ]);
  
  const valueState=Object.values(props?.data);
  const [expanded, setExpanded] = useState(false);

  //  const [fields, setFields] = useState({});
  // useEffect(
  //   () =>
  //     data.forEach((item) =>
  //       setFields((val) => ({ ...val, [item.value]: "" }))
  //     ),
  //   []
  // );

  const deletePath = (value,label,serviceName) => {

const fieldState=[...fields];
fieldState[2].content.map((item,pos)=>{
if(Object.keys(item).includes(serviceName)){
const newPosContent=fieldState[2].content[pos][serviceName].filter((current)=>current.label!==label);
fieldState[2].content[pos]={[serviceName]:[...newPosContent]};
}
});
setFields(fieldState);
  };

  const addPath = () => {
    setPathIndex(val=>val+1);
    const newPath={
      "label": `Path ${pathIndex+1}`,
      "value": "",
      "subAccordion":true,
      "filterBy":"MICROSERVICES",
      "error":false,
      "tooltipContent": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi non quis exercitationem culpa nesciunt nihil aut nostrum explicabo reprehenderit optio amet ab temporibus asperiores quasi cupiditate. Voluptatum ducimus voluptates voluptas?"
    };
    
    const fieldState=[...fields];
    fieldState[2].content.filter(item=>{if(Object.keys(item).includes(`Microservice${index}`)){
     return  item[`Microservice${index}`].push(newPath);
    }})
  setFields(fieldState);
    // setData(newData);
  };

  const handleChange = (e, field,state,filter) =>{
   
    const stateContent=[...fields];
    const context=[...stateContent.filter(item=>item.header===filter)?.[0]?.content];
     const microservices = {...context?.filter(item=>(Object.keys(item).includes(`Microservice${index}`)))};

     Object.values(microservices?.[0])?.[0]?.filter(each=>{
       if(each.label===field) each.value=e.target.value;
     })
    setFields(stateContent)
  }
  
  return (
    <Accordion
      sx={{
        boxShadow: "none",
        background: "#F4F6FA",
        width: "100%",
        minHeight: "50px",
        borderRadius: "6px",
        "&::before": { height: "0" },
      }}
      expanded={expanded}
    >
      <AccordionSummary
        aria-controls="panel1d-content"
        id="panel1d-header"
        expandIcon={
          <ArrowForwardIosSharpIcon
            style={{ fontSize: "14px", color: "red" }}
          />
        }
        sx={{
          display: "flex",
          gap: "8px",
          flexDirection: "row-reverse",
          "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
            transform: "rotate(90deg)",
          },
        }}
        onClick={() => setExpanded((val) => !val)}
      >
        <div
          style={{
            display: "flex",
            gap: "8px",
            alignItems: "center",
            justifyContent: "space-between",
            fontWeight: "700",
            width: "100%",
          }}
        >
          <Typography style={{ fontWeight: 700, fontSize: "14px" }}>
            MICROSERVICE
          </Typography>

          {expanded && index!==1 && (
            <DeleteIcon
              removeMicroservice={props.removeMicroservice}
              index={props.index}
              style={{ justifySelf: "end" }}
            />
          )}
        </div>
      </AccordionSummary>
      <AccordionDetails>
        {valueState && (valueState
         .map((each)=>each)
        .map((fields, index) => (

           fields.map((field,indexPos)=>{
            return <InputField
            data={field}
            key={indexPos}
            position={indexPos}
            handleChange={handleChange}
            inSubAccordion={true}
            value={field.value}
            error={field.error}
            fields={fields}
            deletePath={deletePath}
            isPath={field.label === "Name" ? false : true}
            pathNum={field.label === "Name" ? null : index}
            microserviceName={`Microservice${props.index}`}
          />
           })
         
        
        )))}
        <div
          style={{ display: "flex", justifyContent: "end", marginTop: "16px" }}
        >
          <Button
            sx={{ alignSelf: "end", fontWeight: "700" }}
            style={{ alignSelf: "start", textAlign: "end" }}
            onClick={addPath}
          >
            + Add Path
          </Button>
        </div>
      </AccordionDetails>
    </Accordion>
  );
}

export default Index;
