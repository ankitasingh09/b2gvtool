import React from "react";
import DeleteForeverRoundedIcon from "@mui/icons-material/DeleteForeverRounded";

function DeleteIcon(props) {
  return (
    <div>
      <DeleteForeverRoundedIcon
        color="red"
        sx={{ color: "red", cursor: "pointer" }}
        onClick={
          props.deletePath
            ? () => props.deletePath(props.value,props.label,props.microserviceName)
            : () => props.removeMicroservice(props.index)
        }
      />
    </div>
  );
}

export default DeleteIcon;
