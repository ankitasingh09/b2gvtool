import React, { useState } from "react";
import PropTypes from "prop-types";
import Accordion from "./accordion-element";
import styled from "styled-components";


const AccordionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: 24px;
  align-items: center;
  width: 100%;
  margin-top: 24px;
`;
const Container = ({ openDrawer,fields,setFields,buttonState,setButtonState }) => {
  const [activeAccordion, setActiveAccordion] = useState(0);

  const activateNext = () => {
    setActiveAccordion((val) => val + 1);
  };

  return (
    <AccordionWrapper>
      {fields.map((item, index) => {
        return (
          <Accordion
            key={index}
            index={index}
            data={item}
            activeAccordion={activeAccordion}
            active={index === activeAccordion}
            activateNext={activateNext}
            setActiveAccordion={setActiveAccordion}
            isMicroservicesAccordion={item.header === "MICROSERVICES"}
            openDrawer={openDrawer}
            setFields={setFields}
            fields={fields}
            header={item.header}
            buttonState={buttonState}
            setButtonState={setButtonState}
          />
        );
      })}
    </AccordionWrapper>
  
  );
};

Container.propTypes = {
  fields: PropTypes.array,
  setFields: PropTypes.func,
};

export default Container;
