import React, { useState, useEffect } from "react";
import vector from "../../../../assets/Vector.png";
import remove from "../../../../assets/remove.png";
import "./index.css";

const DataStateContainer = ({projectId,pipelineId}) => {
  const [response,setResponse]=useState("inProgress")
  
  useEffect(()=>{
    getPipelineStatus();
  },[])

  const getPipelineStatus=()=>{
    const url=`https://gitlab.com/api/v4/projects/${projectId}/pipelines/${pipelineId}`
  
    fetch(url, {
      method: 'GET',
      headers: {
      'PRIVATE-TOKEN':process.env.REACT_APP_TOKEN,
      'Content-Type': 'application/json',
   },
   })
   .then(res=>res.json())
   .then(data=>{
     if(data.status==="failed"||data.status==="success")setResponse(data?.status?.toUpperCase())});
  }

  return (
    <div>
      <div className="sanitizationBox">
       
          {response==="SUCCESS" && <div className="mainBox">
            <img src={vector} alt="logo" className="checkimage"></img>
            {/* <div className="percentSanitized">Percent Sanitized</div> */}
            <div className="sanitizationComplete">Sanitization Complete</div>
          </div>}

          {response==="FAILED" && <div className="mainBox">
            <img src={remove} alt="logo" className="checkimage"></img>
            {/* <div className="percentSanitized">Percent Sanitized</div> */}
            <div className="sanitizationComplete">Sanitization Failed</div>
          </div>}
        
          {response==="inProgress" && <div className="mainBox">
            <div className="loader"></div>
            {/* <div className="percentSanitized">Sanitization in Progress</div> */}
            <div className="sanitizationComplete">Sanitization in Progress</div>
          </div>}
        {/* )} */}
      </div>
    </div>
  );
};

export default DataStateContainer;
