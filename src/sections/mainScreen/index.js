import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import Container from "./components/container";
import SanitizeContainer from "./components/container/sanitizeContainer";
import DataStateContainer from "./components/dataStateContainer";
import DownloadContainer from "./components/downloadContainer";
import ProgressBar from "./components/progressBar";
import SubHeader from "./components/subHeader";
import { Buffer } from "buffer";
import "./index.css";
const data = require("./dataLabels/data.json");

function useQuery() {
  const { search } = useLocation();
  return React.useMemo(() => new URLSearchParams(search), [search]);
}

const MainScreen = () => {
  let query = useQuery();
  const queryValue = query.get("pipelineId");
  const projectId = query.get("projectId");
  const jobId = query.get("jobId");
  const [fields, setFields] = useState([...data]);
  const [statusField, setStatusField] = useState({
    Import: jobId ? "completed" : !queryValue ? "active" : "completed",
    Sanitize: jobId ? "completed" : !queryValue ? "Inactive" : "active",
    Download: jobId ? "active" : "inactive",
  });
  const [buttonState, setButtonState] = useState({
    name: jobId ? "DOWNLOAD" : !queryValue ? "IMPORT REPO" : "CONTINUE",
    disabledState: jobId ? false : !queryValue ? true : false,
  });
  const [errorState, setErrorState] = useState(false);
  const [authError, setAuthError] = useState(false);

  useEffect(() => {
    if (queryValue != null) {
      fetchValidateUserEmailToDownload();
    }
  }, [queryValue]);

  const fetchValidateUserEmailToDownload = () => {
    fetch(
      "https://relic-npeprd-us-east-1.prod-services.t-mobile.com/v1/kv/tmobile/APM0223552/DU0248579/mohammed.edhi1@t-mobile.com",
      {
        method: "GET",
        headers: {
          "X-Consul-Token": "a671ea8b-763b-7a73-b098-d2061a71fe2a",
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => {
        authenticateUser();
      })
      .catch((error) => {
        console.log(error);
        setAuthError(true);
        setButtonState({ name: "DOWNLOAD", disabledState: true });
      });
  };

  const authenticateUser = () => {
    let response = [
      {
        LockIndex: 0,
        Key: "tmobile/APM0223552/DU0248579/mohammed.edhi1@t-mobile.com",
        Flags: 0,
        Value: "NDg4NzI2OTY3",
        Namespace: "default",
        CreateIndex: 1179401,
        ModifyIndex: 1372461,
      },
    ];

    var decodedStringAtoB = Buffer.from(response[0].Value).toString("base64");

    if (decodedStringAtoB === queryValue) {
      setAuthError(false);
      console.log("Value matches");
    } else {
      setAuthError(true);
      console.log("value not matches");
      setButtonState({ name: "DOWNLOAD", disabledState: true });
    }
  };

  return (
    <div className="mainScreen-wrapper">
      <SubHeader
        buttonState={buttonState}
        setButtonState={setButtonState}
        statusField={statusField}
        setStatusField={setStatusField}
        fields={fields}
        setErrorState={setErrorState}
      />
      {buttonState?.name !== "IMPORT REPO" && (
        <ProgressBar statusField={statusField} />
      )}
      {buttonState?.name === "IMPORT REPO" && queryValue === null && (
        <Container
          buttonState={buttonState}
          setButtonState={setButtonState}
          fields={fields}
          setFields={setFields}
        />
      )}
      {buttonState?.name === "SANITIZE" && queryValue === null && (
        <SanitizeContainer
          fields={fields}
          setButtonState={setButtonState}
          errorState={errorState}
          setErrorState={setErrorState}
        />
      )}
      {buttonState?.name === "CONTINUE" && queryValue && (
        <DataStateContainer
          buttonState={buttonState}
          setButtonState={setButtonState}
          projectId={projectId}
          pipelineId={queryValue}
        />
      )}
      {buttonState?.name === "DOWNLOAD" && jobId && (
        <DownloadContainer authError={authError} setAuthError={setAuthError} />
      )}
    </div>
  );
};

export default MainScreen;
