import React from "react";
import { useOktaAuth } from "@okta/okta-react";
import "./index.css";

function AuthorizationScreen() {
  const { authState, oktaAuth } = useOktaAuth();
  const loginWithRedirect = () =>
    oktaAuth.signInWithRedirect({ originalUri: "/sanitize" });
  const logOut = () => oktaAuth.signOut();
  const btnLogic = authState?.isAuthenticated ? logOut : loginWithRedirect;

  return (
    <div className="authentication-wrapper">
      <div className="authentication-container">
        <div className="sign-in-text">Sign In</div>
        <div className="log-in-text">Log in using your NTID</div>
        <button className="button-component-auth" onClick={btnLogic}>
          Log In
        </button>
      </div>
    </div>
  );
}

export default AuthorizationScreen;
