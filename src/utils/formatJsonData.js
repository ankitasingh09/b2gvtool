const formatJsonData = (bitbucketRepo, directoryStructure, Microservice) => {
  const structureMicroservice = (service) => {
    const serviceObj = {};
    service.content.forEach((item) => {
      let label,
        values = [];
      Object.values(item)[0].map((item) => {
        if (item.label === "Name") label = item.value;
        else values.push(item.value);
        return null;
      });
      serviceObj[label] = values.join(",");
    });

    return serviceObj;
  };
  return JSON.stringify({
    base: {
      log_level: bitbucketRepo.content[3].value,
      root: directoryStructure.content[0].value,
      santized_dir: directoryStructure.content[2].value,
      log_dir: directoryStructure.content[3].value,
      common_dir: directoryStructure.content[1].value,
      env: bitbucketRepo.content[4].value,
    },
    sanitize: {
      deny_list: directoryStructure.content[4].value,
    },
    microservices: structureMicroservice(Microservice),
  });
};
export default formatJsonData;
