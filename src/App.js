import "./App.css";
import {Route,Switch } from "react-router-dom";
import AuthorizationScreen from "./sections/authorizationScreen";
import { Security, LoginCallback, SecureRoute } from "@okta/okta-react";
import { OktaAuth, toRelativeUrl } from "@okta/okta-auth-js";
import { config } from "./config";
import HeaderLayout from "./components/headerLayout";
import MainScreen from "./sections/mainScreen";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";

function App() {
  const oktaAuth = new OktaAuth(config);
  let navigate = useHistory();
  
  const customAuthHandler = () => {
    navigate.push("/");
  };
  const restoreOriginalUri = async (_oktaAuth, originalUri) => {
    navigate.replace(toRelativeUrl(originalUri, window.location.origin));
  };
  return (
    <div className="App">
      <Security
        oktaAuth={oktaAuth}
        onAuthRequired={customAuthHandler}
        restoreOriginalUri={restoreOriginalUri}
      >
        <SecureRoute path={["/sanitize"]} component={HeaderLayout} />
        <Switch>
          <Route exact path="/" component={AuthorizationScreen} />
          <SecureRoute path="/sanitize" component={MainScreen} />
          <Route path="/login/callback" component={LoginCallback} />
        </Switch>
      </Security>
    </div>
  );
}

export default App;
