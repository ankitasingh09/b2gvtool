const config = {
    issuer:process.env.REACT_APP_OKTA_ISSUER,
    clientId: process.env.REACT_APP_CLIENT_ID,
    redirectUri:'http://localhost:3000/login/callback',
    scopes: ['openid', 'profile', 'email'],
    pkce: true
  };

export {config}  